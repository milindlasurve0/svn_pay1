import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.util.Random;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;
import javax.microedition.rms.RecordStoreNotOpenException;
import javax.wireless.messaging.Message;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.MessageListener;
import javax.wireless.messaging.TextMessage;

import com.sun.lwuit.Dialog;
import com.sun.lwuit.Image;

public class General {

	/**
	 * constants for platforms names
	 */
	public static final String PLATFORM_MOTOROLA = "motorola";
	public static final String PLATFORM_NOKIA = "nokia";
	public static final String PLATFORM_SONY_ERICSSON = "SE";
	public static final String PLATFORM_SIEMENS = "siemens";
	public static final String PLATFORM_SAMSUNG = "samsung";
	public static final String PLATFORM_LG = "LG";
	public static final String PLATFORM_NOT_DEFINED = "NA";
	/**
	 * constants for keycodes
	 */
	public static final int SOFT_KEY_LEFT = -201;
	public static final int SOFT_KEY_RIGHT = -202;
	public static final int SOFT_KEY_MIDDLE_INTERNET = -203;

	/**
	 * Returns mobile phone platform
	 * 
	 * @return name mobile phone platform
	 */
	public static String getPlatform() {
		// detecting NOKIA or SonyEricsson
		try {
			final String currentPlatform = System
					.getProperty("microedition.platform");
			if (currentPlatform.indexOf("Nokia") != -1) {
				return PLATFORM_NOKIA;
			} else if (currentPlatform.indexOf("SonyEricsson") != -1) {
				return PLATFORM_SONY_ERICSSON;
			}
		} catch (Throwable ex) {
		}
		// detecting SAMSUNG
		try {
			Class.forName("com.samsung.util.Vibration");
			return PLATFORM_SAMSUNG;
		} catch (Throwable ex) {
		}
		// detecting MOTOROLA
		try {
			Class.forName("com.motorola.multimedia.Vibrator");
			return PLATFORM_MOTOROLA;
		} catch (Throwable ex) {
			try {
				Class.forName("com.motorola.graphics.j3d.Effect3D");
				return PLATFORM_MOTOROLA;
			} catch (Throwable ex2) {
				try {
					Class.forName("com.motorola.multimedia.Lighting");
					return PLATFORM_MOTOROLA;
				} catch (Throwable ex3) {
					try {
						Class.forName("com.motorola.multimedia.FunLight");
						return PLATFORM_MOTOROLA;
					} catch (Throwable ex4) {
					}
				}
			}
		}
		// detecting SIEMENS
		try {
			Class.forName("com.siemens.mp.io.File");
			return PLATFORM_SIEMENS;
		} catch (Throwable ex) {
		}
		// detecting LG
		try {
			Class.forName("mmpp.media.MediaPlayer");
			return PLATFORM_LG;
		} catch (Throwable ex) {
			try {
				Class.forName("mmpp.phone.Phone");
				return PLATFORM_LG;
			} catch (Throwable ex1) {
				try {
					Class.forName("mmpp.lang.MathFP");
					return PLATFORM_LG;
				} catch (Throwable ex2) {
					try {
						Class.forName("mmpp.media.BackLight");
						return PLATFORM_LG;
					} catch (Throwable ex3) {
					}
				}
			}
		}
		return PLATFORM_NOT_DEFINED;
	}

	// Record Store Name
	public static final String PLAN_RMS = "Plans";
	public static final String ENTERTAINMENT_RMS = "Entertainment";
	public static final String NOTIFICATION_RMS = "Notifications";
	public static final String SETTINGS_RMS = "MySMSSetting";
	public static final String VMN_RMS = "VMN";
	public static final String CHAT_RMS = "ChatHistory";
	public static final String UUID_RMS = "MyUUID";
	public static final String MOBILE_RMS = "MyMobile";
	public static final String COOKIE_RMS = "MyCookie";
	public static final String PROFILE_RMS = "MyProfile";
	public static final String VERSION_RMS = "AppVersion";

	// Entertainment RMS Fields
	/** ID Length 10 **/
	public final static int ENTERTAINMENT_ID_INDEX = 0;
	/** Product ID Length 10 **/
	public final static int ENTERTAINMENT_PRODUCT_ID_INDEX = 10;
	/** Product Code Length 50 **/
	public final static int ENTERTAINMENT_PRODUCT_CODE_INDEX = 20;
	/** Product Name Length 100 **/
	public final static int ENTERTAINMENT_PRODUCT_NAME_INDEX = 70;
	/** Product Price Length 10 **/
	public final static int ENTERTAINMENT_PRODUCT_PRICE_INDEX = 170;
	/** Product Validity Length 20 **/
	public final static int ENTERTAINMENT_PRODUCT_VALIDITY_INDEX = 180;
	/** Product Short Description Length 100 **/
	public final static int ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION_INDEX = 200;
	/** Product Long Description Length 1000 **/
	public final static int ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION_INDEX = 300;
	/** Product Image Length 100 **/
	public final static int ENTERTAINMENT_PRODUCT_IMAGE_INDEX = 1300;
	/** Product Data Field Length 10 **/
	public final static int ENTERTAINMENT_PRODUCT_DATA_FIELD_INDEX = 1400;
	/** Product Data Type Length 10 **/
	public final static int ENTERTAINMENT_PRODUCT_DATA_TYPE_INDEX = 1410;
	/** Product Data Length 10 **/
	public final static int ENTERTAINMENT_PRODUCT_DATA_LENGTH_INDEX = 1420;
	/** Product LastUpdateTime Length 20 **/
	public final static int ENTERTAINMENT_PRODUCT_UPDATE_TIME_INDEX = 1430;
	/** Total Entertainment Length 1450 **/
	public final static int MAX_ENTERTAINMENT_PRODUCT_LEN = 1450;

	public static StringBuffer ENTERTAINMENT_RECORD_BUFFER = new StringBuffer(
			MAX_ENTERTAINMENT_PRODUCT_LEN);

	// Entertainment RMS Creation
	public static byte[] createEntertainmentRecord(String id,
			String product_id, String product_code, String product_name,
			String product_price, String product_validity,
			String short_description, String long_description,
			String product_image, String data_field, String data_type,
			String data_length, String time) {
		clearEntertainmentBuffer();
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_ID_INDEX, id);
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_PRODUCT_ID_INDEX,
				product_id);
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_PRODUCT_CODE_INDEX,
				product_code);
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_PRODUCT_NAME_INDEX,
				product_name);
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_PRODUCT_PRICE_INDEX,
				product_price);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_VALIDITY_INDEX, product_validity);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION_INDEX,
				short_description);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION_INDEX, long_description);
		ENTERTAINMENT_RECORD_BUFFER.insert(ENTERTAINMENT_PRODUCT_IMAGE_INDEX,
				product_image);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_DATA_FIELD_INDEX, data_field);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_DATA_TYPE_INDEX, data_type);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_DATA_LENGTH_INDEX, data_length);
		ENTERTAINMENT_RECORD_BUFFER.insert(
				ENTERTAINMENT_PRODUCT_UPDATE_TIME_INDEX, time);
		ENTERTAINMENT_RECORD_BUFFER.setLength(MAX_ENTERTAINMENT_PRODUCT_LEN);
		return ENTERTAINMENT_RECORD_BUFFER.toString().getBytes();
	}

	// Get ID
	public static String getID(byte[] b) {
		return new String(b, ENTERTAINMENT_ID_INDEX, 10).trim();
	}

	// Get Product ID
	public static String getProductID(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_ID_INDEX, 10).trim();
	}

	// Get Product Code
	public static String getProductCode(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_CODE_INDEX, 50).trim();
	}

	// Get Product Name
	public static String getProductName(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_NAME_INDEX, 100).trim();
	}

	// Get Product Price
	public static String getProductPrice(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_PRICE_INDEX, 10).trim();
	}

	// Get Product Validity
	public static String getProductValidity(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_VALIDITY_INDEX, 20).trim();
	}

	// Get Product Short Description
	public static String getProductShortDescription(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_SHORT_DESCRIPTION_INDEX, 100)
				.trim();
	}

	// Get Product Long Description
	public static String getProductLongDescription(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_LONG_DESCRIPTION_INDEX, 1000)
				.trim();
	}

	// Get Product Image
	public static String getProductImage(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_IMAGE_INDEX, 100).trim();
	}

	// Get Product Data Field
	public static String getProductDataField(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_DATA_FIELD_INDEX, 10).trim();
	}

	// Get Product Data Type
	public static String getProductDataType(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_DATA_TYPE_INDEX, 10).trim();
	}

	// Get Product Data Length
	public static String getProductDataLength(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_DATA_LENGTH_INDEX, 10)
				.trim();
	}

	// Get Product Last Update Time
	public static String getProductLastUpdateTime(byte[] b) {
		return new String(b, ENTERTAINMENT_PRODUCT_UPDATE_TIME_INDEX, 20)
				.trim();
	}

	// Clear internal buffer
	public static void clearEntertainmentBuffer() {
		for (int i = 0; i < MAX_ENTERTAINMENT_PRODUCT_LEN; i++) {
			ENTERTAINMENT_RECORD_BUFFER.insert(i, ' ');
		}
		ENTERTAINMENT_RECORD_BUFFER.setLength(MAX_ENTERTAINMENT_PRODUCT_LEN);
	}

	public static void setEntertainment(String product_id, String product_code,
			String product_name, String product_price, String product_validity,
			String short_description, String long_description,
			String product_image, String data_field, String data_type,
			String data_length, String time) {
		try {
			RecordStore EntertainmentRMS = RecordStore.openRecordStore(
					ENTERTAINMENT_RMS, true);

			byte[] data = createEntertainmentRecord("0", product_id,
					product_code, product_name, product_price,
					product_validity, short_description, long_description,
					product_image, data_field, data_type, data_length, time);
			int ID = EntertainmentRMS.addRecord(data, 0, data.length);
			data = EntertainmentRMS.getRecord(ID);
			data = createEntertainmentRecord(String.valueOf(ID),
					getProductID(data), getProductCode(data),
					getProductName(data), getProductPrice(data),
					getProductValidity(data), getProductShortDescription(data),
					getProductLongDescription(data), getProductImage(data),
					getProductDataField(data), getProductDataType(data),
					getProductDataLength(data), getProductLastUpdateTime(data));
			EntertainmentRMS.setRecord(ID, data, 0, data.length);

			EntertainmentRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	public static void deleteEntertainment() {
		try {
			boolean flag = false;
			RecordStore EntertainmentRMS = RecordStore.openRecordStore(
					ENTERTAINMENT_RMS, true);
			RecordEnumeration re = null;

			try {
				re = EntertainmentRMS.enumerateRecords(null, null, true);
			} catch (RecordStoreNotOpenException e) {
				// TODO: handle exception
			}

			if (EntertainmentRMS.getNumRecords() > 0) {
				byte[] data = re.nextRecord();
				String LastUpdateTime = getProductLastUpdateTime(data);
				int days = (int) ((System.currentTimeMillis() - Long
						.parseLong(LastUpdateTime)) / (1000 * 60 * 60 * 24));

				// System.out.println("Entertainment Data inserted " + days
				// + " days ago.");

				if (days >= 2)
					flag = true;
			}
			re.destroy();
			EntertainmentRMS.closeRecordStore();

			if (flag)
				RecordStore.deleteRecordStore(ENTERTAINMENT_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	// Notification RMS Fields
	/** Notification ID Length 10 **/
	public final static int NOTIFICATION_ID_INDEX = 0;
	/** Notification Data Length 100 **/
	public final static int NOTIFICATION_DATA_INDEX = 10;
	/** Notification Time Length 20 **/
	public final static int NOTIFICATION_TIME_INDEX = 110;
	/** Notification Type Length 20 **/
	public final static int NOTIFICATION_TYPE_INDEX = 130;
	/** Total Notification Length 150 **/
	public final static int MAX_NOTIFICATION_LEN = 150;

	public static StringBuffer NOTIFICATION_RECORD_BUFFER = new StringBuffer(
			MAX_NOTIFICATION_LEN);

	// Notification RMS Creation
	public static byte[] createNotificationRecord(String ID, String Data,
			String Time, String Type) {
		clearNotificationBuffer();
		NOTIFICATION_RECORD_BUFFER.insert(NOTIFICATION_ID_INDEX, ID);
		NOTIFICATION_RECORD_BUFFER.insert(NOTIFICATION_DATA_INDEX, Data);
		NOTIFICATION_RECORD_BUFFER.insert(NOTIFICATION_TIME_INDEX, Time);
		NOTIFICATION_RECORD_BUFFER.insert(NOTIFICATION_TYPE_INDEX, Type);
		NOTIFICATION_RECORD_BUFFER.setLength(MAX_NOTIFICATION_LEN);
		return NOTIFICATION_RECORD_BUFFER.toString().getBytes();
	}

	// Get Notification ID
	public static String getNotificationID(byte[] b) {
		return new String(b, NOTIFICATION_ID_INDEX, 10).trim();
	}

	// Get Notification Data
	public static String getNotificationData(byte[] b) {
		return new String(b, NOTIFICATION_DATA_INDEX, 100).trim();
	}

	// Get Notification Time
	public static String getNotificationTime(byte[] b) {
		return new String(b, NOTIFICATION_TIME_INDEX, 20).trim();
	}

	// Get Notification Type
	public static String getNotificationType(byte[] b) {
		return new String(b, NOTIFICATION_TYPE_INDEX, 20).trim();
	}

	// Clear internal buffer
	public static void clearNotificationBuffer() {
		for (int i = 0; i < MAX_NOTIFICATION_LEN; i++) {
			NOTIFICATION_RECORD_BUFFER.insert(i, ' ');
		}
		NOTIFICATION_RECORD_BUFFER.setLength(MAX_NOTIFICATION_LEN);
	}

	public static void setNotification(String Data, String Time, String Type) {
		try {
			RecordStore NotificationRMS = RecordStore.openRecordStore(
					NOTIFICATION_RMS, true);

			byte[] data = createNotificationRecord("0", Data, Time, Type);
			int ID = NotificationRMS.addRecord(data, 0, data.length);
			data = NotificationRMS.getRecord(ID);
			data = createNotificationRecord(String.valueOf(ID),
					getNotificationData(data), getNotificationTime(data),
					getNotificationType(data));
			NotificationRMS.setRecord(ID, data, 0, data.length);

			NotificationRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	// VMN RMS Fields
	/** VMN ID Length 10 **/
	public final static int VMN_ID_INDEX = 0;
	/** VMN Data Length 20 **/
	public final static int VMN_DATA_INDEX = 10;
	/** Total VMN Length 30 **/
	public final static int MAX_VMN_LEN = 30;

	public static StringBuffer VMN_RECORD_BUFFER = new StringBuffer(MAX_VMN_LEN);

	// VMN RMS Creation
	public static byte[] createVMNRecord(String ID, String Data) {
		clearVMNBuffer();
		VMN_RECORD_BUFFER.insert(VMN_ID_INDEX, ID);
		VMN_RECORD_BUFFER.insert(VMN_DATA_INDEX, Data);
		VMN_RECORD_BUFFER.setLength(MAX_VMN_LEN);
		return VMN_RECORD_BUFFER.toString().getBytes();
	}

	// Get VMN ID
	public static String getVMNID(byte[] b) {
		return new String(b, VMN_ID_INDEX, 10).trim();
	}

	// Get VMN Data
	public static String getVMNData(byte[] b) {
		return new String(b, VMN_DATA_INDEX, 20).trim();
	}

	// Clear internal buffer
	public static void clearVMNBuffer() {
		for (int i = 0; i < MAX_VMN_LEN; i++) {
			VMN_RECORD_BUFFER.insert(i, ' ');
		}
		VMN_RECORD_BUFFER.setLength(MAX_VMN_LEN);
	}

	public static void setVMN(String Data) {
		try {
			RecordStore VMNRMS = RecordStore.openRecordStore(VMN_RMS, true);

			byte[] data = createVMNRecord("0", Data);
			int ID = VMNRMS.addRecord(data, 0, data.length);
			data = VMNRMS.getRecord(ID);
			data = createVMNRecord(String.valueOf(ID), getVMNData(data));
			VMNRMS.setRecord(ID, data, 0, data.length);

			VMNRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	public static String[] getVMN() {
		String[] data = null;
		try {
			RecordStore VMNRMS = RecordStore.openRecordStore(VMN_RMS, true);

			RecordEnumeration re = null;
			try {
				re = VMNRMS.enumerateRecords(null, null, true);
			} catch (RecordStoreNotOpenException e) {
				// TODO: handle exception
			}
			data = new String[re.numRecords()];
			int i = 0;
			while (re.hasNextElement()) {
				byte[] b = re.nextRecord();
				data[i] = General.getVMNData(b);
				i++;
			}

			re.destroy();
			VMNRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
		return data;
	}

	public static void deleteVMN() {
		try {
			RecordStore.deleteRecordStore(VMN_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}

	}

	public static String selectRandomVMN() {
		int i = 0;
		String[] vmn_numbers = General.getVMN();
		try {
			int length = vmn_numbers.length;

			Random r = new Random();
			i = r.nextInt(length - 0) + 0;
			// System.out.println("Len " + length + " Random " + i
			// + " Selected VMN " + vmn_numbers[i]);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return vmn_numbers[i];
	}

	// Chat RMS Fields
	/** Chat ID Length 10 **/
	public final static int CHAT_ID_INDEX = 0;
	/** Chat Data Length 1000 **/
	public final static int CHAT_DATA_INDEX = 10;
	/** Chat Time Length 20 **/
	public final static int CHAT_TIME_INDEX = 1010;
	/** Chat Position Length 10 **/
	public final static int CHAT_POSITION_INDEX = 1030;
	/** Total VMN Length 1040 **/
	public final static int MAX_CHAT_LEN = 1040;

	public static StringBuffer CHAT_RECORD_BUFFER = new StringBuffer(
			MAX_CHAT_LEN);

	// VMN RMS Creation
	public static byte[] createChatRecord(String ID, String Data, String Time,
			String Position) {
		clearChatBuffer();
		CHAT_RECORD_BUFFER.insert(CHAT_ID_INDEX, ID);
		CHAT_RECORD_BUFFER.insert(CHAT_DATA_INDEX, Data);
		CHAT_RECORD_BUFFER.insert(CHAT_TIME_INDEX, Time);
		CHAT_RECORD_BUFFER.insert(CHAT_POSITION_INDEX, Position);
		CHAT_RECORD_BUFFER.setLength(MAX_CHAT_LEN);
		return CHAT_RECORD_BUFFER.toString().getBytes();
	}

	// Get Chat ID
	public static String getChatID(byte[] b) {
		return new String(b, CHAT_ID_INDEX, 10).trim();
	}

	// Get Chat Data
	public static String getChatData(byte[] b) {
		return new String(b, CHAT_DATA_INDEX, 1000).trim();
	}

	// Get Chat Time
	public static String getChatTime(byte[] b) {
		return new String(b, CHAT_TIME_INDEX, 20).trim();
	}

	// Get Chat Position
	public static String getChatPosition(byte[] b) {
		return new String(b, CHAT_POSITION_INDEX, 10).trim();
	}

	// Clear internal buffer
	public static void clearChatBuffer() {
		for (int i = 0; i < MAX_CHAT_LEN; i++) {
			CHAT_RECORD_BUFFER.insert(i, ' ');
		}
		CHAT_RECORD_BUFFER.setLength(MAX_CHAT_LEN);
	}

	public static void setChat(String Data, String Time, String Position) {
		try {
			RecordStore ChatRMS = RecordStore.openRecordStore(CHAT_RMS, true);

			byte[] data = createChatRecord("0", Data, Time, Position);
			int ID = ChatRMS.addRecord(data, 0, data.length);
			data = ChatRMS.getRecord(ID);
			data = createChatRecord(String.valueOf(ID), getChatData(data),
					getChatTime(data), getChatPosition(data));
			ChatRMS.setRecord(ID, data, 0, data.length);

			ChatRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	public static void deleteChat() {
		try {
			RecordStore.deleteRecordStore(CHAT_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}

	}

	// Plan Fields
	/** Plan ID Length 10 **/
	public final static int PLAN_ID_INDEX = 0;
	/** Plan Operator ID Length 10 **/
	public final static int PLAN_OPERATOR_ID_INDEX = 10;
	/** Plan Circle ID Length 10 **/
	public final static int PLAN_CIRCLE_ID_INDEX = 20;
	/** Plan Type Length 100 **/
	public final static int PLAN_TYPE_INDEX = 30;
	/** Plan Amount Length 10 **/
	public final static int PLAN_AMOUNT_INDEX = 130;
	/** Plan Validity Length 10 **/
	public final static int PLAN_VALIDITY_INDEX = 140;
	/** Plan Description Length 1000 **/
	public final static int PLAN_DESCRIPTION_INDEX = 150;
	/** Plan LastUpdateTime Length 20 **/
	public final static int PLAN_UPDATE_TIME_INDEX = 1150;
	/** Total Plan Length 1170 **/
	public final static int MAX_PLAN_LEN = 1170;

	public static StringBuffer PLAN_RECORD_BUFFER = new StringBuffer(
			MAX_PLAN_LEN);

	// Plan RMS Creation
	public static byte[] createPlanRecord(String ID, String OperatorID,
			String CircleID, String PlanType, String Amount, String Validity,
			String Description, String LastUpdateTime) {
		clearPlanBuffer();
		PLAN_RECORD_BUFFER.insert(PLAN_ID_INDEX, ID);
		PLAN_RECORD_BUFFER.insert(PLAN_OPERATOR_ID_INDEX, OperatorID);
		PLAN_RECORD_BUFFER.insert(PLAN_CIRCLE_ID_INDEX, CircleID);
		PLAN_RECORD_BUFFER.insert(PLAN_TYPE_INDEX, PlanType);
		PLAN_RECORD_BUFFER.insert(PLAN_AMOUNT_INDEX, Amount);
		PLAN_RECORD_BUFFER.insert(PLAN_VALIDITY_INDEX, Validity);
		PLAN_RECORD_BUFFER.insert(PLAN_DESCRIPTION_INDEX, Description);
		PLAN_RECORD_BUFFER.insert(PLAN_UPDATE_TIME_INDEX, LastUpdateTime);
		PLAN_RECORD_BUFFER.setLength(MAX_PLAN_LEN);
		return PLAN_RECORD_BUFFER.toString().getBytes();
	}

	// Get Plan ID
	public static String getPlanID(byte[] b) {
		return new String(b, PLAN_ID_INDEX, 10).trim();
	}

	// Get Plan Operator ID
	public static String getPlanOperatorID(byte[] b) {
		return new String(b, PLAN_OPERATOR_ID_INDEX, 10).trim();
	}

	// Get Plan Circle ID
	public static String getPlanCircleID(byte[] b) {
		return new String(b, PLAN_CIRCLE_ID_INDEX, 10).trim();
	}

	// Get Plan Type
	public static String getPlanType(byte[] b) {
		return new String(b, PLAN_TYPE_INDEX, 100).trim();
	}

	// Get Plan Amount
	public static String getPlanAmount(byte[] b) {
		return new String(b, PLAN_AMOUNT_INDEX, 10).trim();
	}

	// Get Plan Validity
	public static String getPlanValidity(byte[] b) {
		return new String(b, PLAN_VALIDITY_INDEX, 10).trim();
	}

	// Get Plan Description
	public static String getPlanDescription(byte[] b) {
		return new String(b, PLAN_DESCRIPTION_INDEX, 1000).trim();
	}

	// Get Plan Last Update Time
	public static String getPlanLastUpdateTime(byte[] b) {
		return new String(b, PLAN_UPDATE_TIME_INDEX, 20).trim();
	}

	// Clear internal buffer
	public static void clearPlanBuffer() {
		for (int i = 0; i < MAX_PLAN_LEN; i++) {
			PLAN_RECORD_BUFFER.insert(i, ' ');
		}
		PLAN_RECORD_BUFFER.setLength(MAX_PLAN_LEN);
	}

	public static void setPlan(String OperatorID, String CircleID,
			String PlanType, String Amount, String Validity,
			String Description, String LastUpdateTime) {
		try {
			RecordStore PlanRMS = RecordStore.openRecordStore(PLAN_RMS, true);

			byte[] data = createPlanRecord("0", OperatorID, CircleID, PlanType,
					Amount, Validity, Description, LastUpdateTime);
			int ID = PlanRMS.addRecord(data, 0, data.length);
			data = PlanRMS.getRecord(ID);
			data = createPlanRecord(String.valueOf(ID),
					getPlanOperatorID(data), getPlanCircleID(data),
					getPlanType(data), getPlanAmount(data),
					getPlanValidity(data), getPlanDescription(data),
					getPlanLastUpdateTime(data));
			PlanRMS.setRecord(ID, data, 0, data.length);

			PlanRMS.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	public static void deletePlan() {

	}

	public static String urlEncode(String s) throws IOException {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		DataOutputStream dOut = new DataOutputStream(bOut);
		StringBuffer ret = new StringBuffer(); // return value
		dOut.writeUTF(s);
		ByteArrayInputStream bIn = new ByteArrayInputStream(bOut.toByteArray());
		bIn.read();
		bIn.read();
		int c = bIn.read();
		while (c >= 0) {
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
					|| (c >= '0' && c <= '9') || c == '.' || c == '-'
					|| c == '*' || c == '_') {
				ret.append((char) c);
			} else if (c == ' ') {
				ret.append('+');
			} else {
				if (c < 128) {
					appendHex(c, ret);
				} else if (c < 224) {
					appendHex(c, ret);
					appendHex(bIn.read(), ret);
				} else if (c < 240) {
					appendHex(c, ret);
					appendHex(bIn.read(), ret);
					appendHex(bIn.read(), ret);
				}
			}
			c = bIn.read();
		}
		return ret.toString();
	}

	private static void appendHex(int arg0, StringBuffer buff) {
		buff.append('%');
		if (arg0 < 16) {
			buff.append('0');
		}
		buff.append(Integer.toHexString(arg0));
	}

	public static String getMobile() {
		RecordStore rs;
		String mobile = null;
		try {
			rs = RecordStore.openRecordStore(MOBILE_RMS, true);
			if (rs.getNumRecords() > 0) {
				mobile = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (mobile == null || mobile.equals("")) {
			return "";
		} else {
			return mobile;
		}
	}

	public static void setMobile(String mobile) {
		byte bytes[] = mobile.getBytes();
		RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(MOBILE_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static String getCookie() {
		RecordStore rs;
		String mobile = null;
		try {
			rs = RecordStore.openRecordStore(COOKIE_RMS, true);
			if (rs.getNumRecords() > 0) {
				mobile = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (mobile == null || mobile.equals("")) {
			return "";
		} else {
			return mobile;
		}
	}

	public static void setCookie(String cookie) {
		if (cookie != null) {
		} else {
			cookie = "1234567890";
		}
		byte bytes[] = cookie.getBytes();
		RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(COOKIE_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

	}
	public static void deleteCookie() {
		try {
			RecordStore.deleteRecordStore(COOKIE_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
	public static void deleteMobile() {
		try {
			RecordStore.deleteRecordStore(MOBILE_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static String getVersion() {
		RecordStore rs;
		String version = null;
		try {
			rs = RecordStore.openRecordStore(VERSION_RMS, true);
			if (rs.getNumRecords() > 0) {
				version = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (version == null || version.equals("")) {
			return "";
		} else {
			return version;
		}
	}

	public static void setVersion(String version) {
		byte bytes[] = version.getBytes();
		RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(VERSION_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static void deleteVersion() {
		try {
			RecordStore.deleteRecordStore(VERSION_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static String getUUID() {
		RecordStore rs;
		String uuid = null;
		try {
			rs = RecordStore.openRecordStore(UUID_RMS, true);
			if (rs.getNumRecords() > 0) {
				uuid = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (uuid == null || uuid.equals("")) {
			return "";
		} else {
			return uuid;
		}
	}

	public static void setUUID(String uuid) {
		byte bytes[] = uuid.getBytes();
		RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(UUID_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static void deleteUUID() {
		try {
			RecordStore.deleteRecordStore(UUID_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static String getProfile() {
		RecordStore rs;
		String uuid = null;
		try {
			rs = RecordStore.openRecordStore(PROFILE_RMS, true);
			if (rs.getNumRecords() > 0) {
				uuid = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (uuid == null || uuid.equals("")) {
			return "";
		} else {
			return uuid;
		}
	}

	public static void setProfile(String profile) {
		byte bytes[] = profile.getBytes();
		RecordStore rs;
		try {
			rs = RecordStore.openRecordStore(PROFILE_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static void deleteProfile() {
		try {
			RecordStore.deleteRecordStore(PROFILE_RMS);
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static String generateUUID(String mobile, String pin) {
		int length = 10000;
		long time = System.currentTimeMillis();
		String uuid = null;
		try {
			Random r = new Random();
			int k = r.nextInt(length - 0) + 0;
			uuid = k + time + mobile + pin;
			// System.out.println("UUID " + uuid);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return uuid;
	}

	public static String getSMSSetting() {
		RecordStore rs;
		String setting = null;
		try {
			rs = RecordStore.openRecordStore(SETTINGS_RMS, true);
			if (rs.getNumRecords() > 0) {
				setting = new String(rs.getRecord(1));
			}
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		if (setting == null || setting.equals("")) {
			return "";
		} else {
			return setting;
		}
	}

	public static void setSMSSetting(String flag) {
		byte bytes[] = flag.getBytes();
		RecordStore rs = null;
		try {
			rs = RecordStore.openRecordStore(SETTINGS_RMS, true);
			rs.addRecord(bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static void updateSMSSetting(String flag) {
		byte bytes[] = flag.getBytes();
		RecordStore rs = null;
		try {
			rs = RecordStore.openRecordStore(SETTINGS_RMS, true);
			rs.setRecord(1, bytes, 0, bytes.length);
			rs.closeRecordStore();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	public static void getConnectionInformation(HttpConnection hc) {

		// System.out.println("Request Method for this connection is "
		// + hc.getRequestMethod());
		// System.out.println("URL in this connection is " + hc.getURL());
		// System.out.println("Protocol for this connection is "
		// + hc.getProtocol()); // It better be HTTP:)
		// System.out.println("This object is connected to " + hc.getHost()
		// + " host");
		// System.out.println("HTTP Port in use is " + hc.getPort());
		// System.out.println("Query parameter in this request are  "
		// + hc.getQuery());

	}

	/*
	 * public static double[] getLocation() { Criteria cr = new Criteria();
	 * cr.setHorizontalAccuracy(500);
	 * 
	 * // Get an instance of the provider LocationProvider lp; double lat = 0.0,
	 * lon = 0.0; try { lp = LocationProvider.getInstance(cr); Location l =
	 * lp.getLocation(100); Coordinates c = l.getQualifiedCoordinates();
	 * 
	 * if (c != null) { // Use coordinate information lat = c.getLatitude(); lon
	 * = c.getLongitude(); } } catch (LocationException e) { // TODO
	 * Auto-generated catch block // e.printStackTrace(); } catch
	 * (InterruptedException e) { // TODO Auto-generated catch block //
	 * e.printStackTrace(); } return new double[] { lat, lon }; // Request the
	 * location, setting a one-minute timeout }
	 */

	public static String[] createHTTPConnection(String base_url, String params,
			String cookie) throws IOException {
		cookie = getCookie();
		StringBuffer b = new StringBuffer();
		String str = "";
		HttpConnection c = null;
		InputStream is = null;
		OutputStream os = null;
		// TODO Auto-generated method stub
		try {
			c = (HttpConnection) Connector.open(base_url, Connector.READ_WRITE,
					true);

			long len = 0;
			int ch = 0;

			c.setRequestProperty("User-Agent",
					"Profile/MIDP-2.0 Configuration/CLDC-1.0");
			c.setRequestMethod(HttpConnection.POST);
			// Content-Type is must to pass parameters in POST Request
			c.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			if (cookie != null)
				c.setRequestProperty("cookie", cookie);
			// getConnectionInformation(c);
			os = c.openOutputStream();
			os.write(params.getBytes());

			is = c.openInputStream();
			cookie = c.getHeaderField("set-cookie");
			setCookie(cookie);
			len = c.getLength();

			if (len != -1) {
				// Read exactly Content-Length bytes
				for (int i = 0; i < len; i++) {
					if ((ch = is.read()) != -1) {
						b.append((char) ch);
					}
				}
			} else {
				// Read till the connection is closed.
				while ((ch = is.read()) != -1) {
					len = is.available();
					b.append((char) ch);
				}
			}
			str = b.toString().trim();
			System.out.println("Before " + str);
			str = str.substring(2, str.length() - 3).trim();
			System.out.println("After " + str);
			if (c != null)
				c.close();
			if (is != null)
				is.close();
			if (os != null)
				os.close();
		} catch (IOException e) {
			return new String[] { "Exception", "" };
		} catch (SecurityException e) {
			return new String[] { "Exception", "" };
		} finally {
			if (c != null)
				c.close();
			if (is != null)
				is.close();
			if (os != null)
				os.close();
		}

		return new String[] { str, cookie };
	}

	public static String createPlanHTTPConnection(String base_url, String params)
			throws IOException {
		StringBuffer b = new StringBuffer();
		String str = "";
		HttpConnection c = null;
		InputStream is = null;
		OutputStream os = null;
		// TODO Auto-generated method stub
		try {
			c = (HttpConnection) Connector.open(base_url, Connector.READ_WRITE,
					true);

			long len = 0;
			int ch = 0;

			c.setRequestProperty("User-Agent",
					"Profile/MIDP-2.0 Configuration/CLDC-1.0");
			c.setRequestMethod(HttpConnection.POST);
			// Content-Type is must to pass parameters in POST Request
			c.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			os = c.openOutputStream();
			os.write(params.getBytes());

			is = c.openInputStream();

			len = c.getLength();

			if (len != -1) {
				// Read exactly Content-Length bytes
				for (int i = 0; i < len; i++) {
					if ((ch = is.read()) != -1) {
						b.append((char) ch);
					}
				}
			} else {
				// Read till the connection is closed.
				while ((ch = is.read()) != -1) {
					len = is.available();
					b.append((char) ch);
				}
			}
			str = b.toString().trim();
			System.out.println("Before " + str);
			str = str.substring(2, str.length() - 3).trim();
			System.out.println("After " + str);
			if (c != null)
				c.close();
			if (is != null)
				is.close();
			if (os != null)
				os.close();
		} catch (IOException e) {
			return "Exception";
		} catch (SecurityException e) {
			return "Exception";
		} finally {
			if (c != null)
				c.close();
			if (is != null)
				is.close();
			if (os != null)
				os.close();
		}

		return str;
	}

	public static void sendSMS(String address, String message) {

		try {
			MessageConnection smsConnection = (MessageConnection) Connector
					.open("sms://" + address);

			TextMessage textMessage = (TextMessage) smsConnection
					.newMessage(MessageConnection.TEXT_MESSAGE);
			textMessage.setPayloadText(message);
			address = address.substring(address.length() - 10);
			// textMessage.setAddress(address);

			// System.out.println("SMS Address " + address);
			// Now send the message
			smsConnection.send(textMessage);
			smsConnection.close();

		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	public static void receiveSMS(String address) {
		try {
			final MessageConnection smsConnection = (MessageConnection) Connector
					.open("sms://:50001");

			smsConnection.setMessageListener(new MessageListener() {

				public void notifyIncomingMessage(MessageConnection arg0) {
					// TODO Auto-generated method stub
					Message message = null;
					try {
						message = smsConnection.receive();
					} catch (InterruptedIOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					if (message instanceof TextMessage) {
						TextMessage textMessage = (TextMessage) message;
						Dialog.show("SMS", textMessage.getAddress() + "\n"
								+ textMessage.getPayloadText(), "ok", null);
					} else {
						// Message can be binary or multipart
					}
				}
			});

		} catch (Exception e) {
		}
	}

	public static boolean getMIDPVersionFlag() {

		boolean flag = false;
		try {
			String[] space_split = General.split(
					System.getProperty("microedition.profiles"), "MIDP-");
			for (int i = 0; i < space_split.length; i++) {

				if (Double.parseDouble(space_split[i].trim()) < 2.1) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}

	public static String getMIDPVersion() {
		try {
			String[] space_split = General.split(
					System.getProperty("microedition.profiles"), "MIDP-");
			return space_split[0];
		} catch (Exception e) {
			return "";
		}
	}

	// public static String dataEncryptor(String sInput) {
	//
	// // SHA1 digest length is 160b (40 characters)
	// int nDigestLen = 20;
	// byte[] PlainText = sInput.getBytes();
	// // Allocate memory for the encrypted text
	// byte[] EncryptedText = new byte[nDigestLen];
	// MessageDigest Cipher;
	// try {
	// Cipher = MessageDigest.getInstance("SHA-1");
	// Cipher.update(PlainText, 0, PlainText.length);
	// Cipher.digest(EncryptedText, 0, nDigestLen);
	// } catch (DigestException e) {
	// // TODO: handle exception
	// // e.printStackTrace();
	// } catch (NoSuchAlgorithmException e) {
	// // e.printStackTrace();
	// } catch (Exception e) {
	// // e.printStackTrace();
	// }
	// return toHex(EncryptedText);
	// }
	//
	// private static String toHex(byte[] bsData) {
	// int nDataLen = bsData.length;
	// String sHex = "";
	// for (int nIter = 0; nIter < nDataLen; nIter++) {
	// int nValue = (bsData[nIter] + 256) % 256;
	// int nIndex1 = nValue >> 4;
	// sHex += Integer.toHexString(nIndex1);
	// int nIndex2 = nValue & 0x0f;
	// sHex += Integer.toHexString(nIndex2);
	// }
	// return sHex;
	// }

	public static String[] split(String original, String separator) {
		Vector nodes = new Vector();

		// Parse nodes into vector
		int index = original.indexOf(separator);
		while (index >= 0) {
			String str = original.substring(0, index);
			if (!str.equals(null) && !str.equals("") && str.length() != 0) {
				nodes.addElement(str);
			}
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		// Get the last node
		nodes.addElement(original);
		// Create string array with segments of split string
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = String.valueOf(nodes.elementAt(loop));
			}
		}
		return result;
	}

	private static String Operator = null;
	private static int StvTop = 0;
	private static String RechargeType = null;

	/**
	 * @return the operator
	 */
	public static String getOperator() {
		return Operator;
	}

	/**
	 * @param operator
	 *            the operator to set
	 */
	public static void setOperator(String operator) {
		Operator = operator;
	}

	/**
	 * @return the stvTop
	 */
	public static int getStvTop() {
		return StvTop;
	}

	/**
	 * @param stvTop
	 *            the stvTop to set
	 */
	public static void setStvTop(int stvTop) {
		StvTop = stvTop;
	}

	/**
	 * @return the rechargeType
	 */
	public static String getRechargeType() {
		return RechargeType;
	}

	/**
	 * @param rechargeType
	 *            the rechargeType to set
	 */
	public static void setRechargeType(String rechargeType) {
		RechargeType = rechargeType;
	}

	/**
	 * @param email
	 *            address Check is email address is valid or not
	 */
	public static boolean isValidEmail(String email) {
		email = email.trim();
		String reverse = new StringBuffer(email).reverse().toString();
		if (email == null || email.length() == 0 || email.indexOf("@") == -1) {
			return false;
		}
		int emailLength = email.length();
		int atPosition = email.indexOf("@");
		int atDot = reverse.indexOf(".");

		String beforeAt = email.substring(0, atPosition);
		String afterAt = email.substring(atPosition + 1, emailLength);

		if (beforeAt.length() == 0 || afterAt.length() == 0) {
			return false;
		}
		for (int i = 0; email.length() - 1 > i; i++) {
			char i1 = email.charAt(i);
			char i2 = email.charAt(i + 1);
			if (i1 == '.' && i2 == '.') {
				return false;
			}
		}
		if (email.charAt(atPosition - 1) == '.' || email.charAt(0) == '.'
				|| email.charAt(atPosition + 1) == '.'
				|| afterAt.indexOf("@") != -1 || atDot < 2) {
			return false;
		}

		return true;
	}

	public static Image getBitmapById(int id) {
		try {
			switch (id) {
			case 1:
				return Image.createImage("/images/icons/products/1.png");
			case 2:
				return Image.createImage("/images/icons/products/2.png");
			case 3:
				return Image.createImage("/images/icons/products/3.png");
			case 4:
				return Image.createImage("/images/icons/products/4.png");
			case 5:
				return Image.createImage("/images/icons/products/5.png");
			case 6:
				return Image.createImage("/images/icons/products/6.png");
			case 7:
				return Image.createImage("/images/icons/products/7.png");
			case 8:
				return Image.createImage("/images/icons/products/8.png");
			case 9:
				return Image.createImage("/images/icons/products/9.png");
			case 27:
				return Image.createImage("/images/icons/products/9.png");
			case 10:
				return Image.createImage("/images/icons/products/10.png");
			case 11:
				return Image.createImage("/images/icons/products/11.png");
			case 12:
				return Image.createImage("/images/icons/products/12.png");
			case 13:
				return Image.createImage("/images/icons/products/13.png");
			case 14:
				return Image.createImage("/images/icons/products/14.png");
			case 15:
				return Image.createImage("/images/icons/products/15.png");
			case 16:
				return Image.createImage("/images/icons/products/16.png");
			case 17:
				return Image.createImage("/images/icons/products/17.png");
			case 18:
				return Image.createImage("/images/icons/products/18.png");
			case 19:
				return Image.createImage("/images/icons/products/19.png");
			case 20:
				return Image.createImage("/images/icons/products/20.png");
			case 21:
				return Image.createImage("/images/icons/products/21.png");
			case 22:
				return Image.createImage("/images/icons/products/22.png");
			case 23:
				return Image.createImage("/images/icons/products/23.png");
			case 30:
				return Image.createImage("/images/icons/products/31.png");
			case 35:
				return Image.createImage("/images/icons/products/35.png");
			case 36:
				return Image.createImage("/images/icons/products/9.png");
			case 37:
				return Image.createImage("/images/icons/products/5.png");
			case 38:
				return Image.createImage("/images/icons/products/3.png");
			case 39:
				return Image.createImage("/images/icons/products/4.png");
			case 40:
				return Image.createImage("/images/icons/products/10.png");
			case 41:
				return Image.createImage("/images/icons/products/12.png");
			case 42:
				return Image.createImage("/images/icons/products/2.png");
			case 43:
				return Image.createImage("/images/icons/products/8.png");
			case 44:
				return Image.createImage("/images/icons/products/44.png");
			case 45:
				return Image.createImage("/images/icons/products/45.png");
			case 46:
				return Image.createImage("/images/icons/products/46.png");
			case 47:
				return Image.createImage("/images/icons/products/47.png");
			case 48:
				return Image.createImage("/images/icons/products/48.png");
			case 49:
				return Image.createImage("/images/icons/products/49.png");
			case 50:
				return Image.createImage("/images/icons/products/50.png");
			case 51:
				return Image.createImage("/images/icons/products/51.png");
			default:
				return Image.createImage("/images/icons/products/1.png");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;

	}

}

class SortNotification implements RecordComparator {
	private byte[] recData = new byte[10];

	// Read from a specified byte array
	private ByteArrayInputStream strmBytes = null;

	// Read Java data types from the above byte array
	private DataInputStream strmDataType = null;

	public void compareIntClose() {
		try {
			if (strmBytes != null)
				strmBytes.close();
			if (strmDataType != null)
				strmDataType.close();
		} catch (Exception e) {
		}
	}

	public int compare(byte[] rec1, byte[] rec2) {
		int x1, x2;

		try {
			// If either record is larger than our buffer, reallocate
			int maxsize = Math.max(rec1.length, rec2.length);
			if (maxsize > recData.length)
				recData = new byte[maxsize];

			// Read record #1
			// We want the integer from the record, which is
			// the second "field" thus we must read the
			// String first to get to the integer value
			strmBytes = new ByteArrayInputStream(rec1);
			strmDataType = new DataInputStream(strmBytes);
			strmDataType.readUTF(); // Read string
			x1 = strmDataType.readInt(); // Read integer

			// Read record #2
			strmBytes = new ByteArrayInputStream(rec2);
			strmDataType = new DataInputStream(strmBytes);
			strmDataType.readUTF(); // Read string
			x2 = strmDataType.readInt(); // Read integer

			// Compare record #1 and #2
			if (x1 == x2)
				return RecordComparator.EQUIVALENT;
			else if (x1 < x2)
				return RecordComparator.PRECEDES;
			else
				return RecordComparator.FOLLOWS;

		} catch (Exception e) {
			return RecordComparator.EQUIVALENT;
		}
	}

}

class SortPlanByAmount implements RecordComparator {
	private byte[] recData = new byte[10];

	// Read from a specified byte array
	private ByteArrayInputStream strmBytes = null;

	// Read Java data types from the above byte array
	private DataInputStream strmDataType = null;

	public void compareIntClose() {
		try {
			if (strmBytes != null)
				strmBytes.close();
			if (strmDataType != null)
				strmDataType.close();
		} catch (Exception e) {
		}
	}

	public int compare(byte[] rec1, byte[] rec2) {
		int x1, x2;

		try {
			// If either record is larger than our buffer, reallocate
			int maxsize = Math.max(rec1.length, rec2.length);
			if (maxsize > recData.length)
				recData = new byte[maxsize];

			// Read record #1
			// We want the integer from the record, which is
			// the second "field" thus we must read the
			// String first to get to the integer value
			// strmBytes = new ByteArrayInputStream(rec1);
			// strmDataType = new DataInputStream(strmBytes);
			// strmDataType.readUTF(); // Read string
			// x1 = strmDataType.readInt(); // Read integer

			x1 = Integer.parseInt(General.getPlanAmount(rec1));
			// Read record #2
			// strmBytes = new ByteArrayInputStream(rec2);
			// strmDataType = new DataInputStream(strmBytes);
			// strmDataType.readUTF(); // Read string
			// x2 = strmDataType.readInt(); // Read integer

			x2 = Integer.parseInt(General.getPlanAmount(rec2));

			// Compare record #1 and #2
			if (x1 == x2)
				return RecordComparator.EQUIVALENT;
			else if (x1 < x2)
				return RecordComparator.PRECEDES;
			else
				return RecordComparator.FOLLOWS;

		} catch (Exception e) {
			return RecordComparator.EQUIVALENT;
		}
	}
}

class SearchPlanByOperatorAndCircle implements RecordFilter {
	private String[] searchText = null;

	public SearchPlanByOperatorAndCircle(String[] searchText) {
		// Text to find
		this.searchText = searchText;
	}

	public boolean matches(byte[] candidate) {
		// String str = new String(candidate);
		String operator = searchText[0];
		String circle = searchText[1];
		// Look for text
		if (searchText != null
				&& General.getPlanOperatorID(candidate).indexOf(operator) != -1
				&& General.getPlanCircleID(candidate).indexOf(circle) != -1)
			return true;
		else
			return false;
	}
}

class SearchPlanByType implements RecordFilter {
	private String[] searchText = null;

	public SearchPlanByType(String[] searchText) {
		// Text to find
		this.searchText = searchText;
	}

	public boolean matches(byte[] candidate) {
		// String str = new String(candidate);
		String operator = searchText[0];
		String circle = searchText[1];
		String planType = searchText[2];
		// Look for text
		if (!planType.equalsIgnoreCase("Select Type")) {
			if (searchText != null
					&& General.getPlanOperatorID(candidate).indexOf(operator) != -1
					&& General.getPlanCircleID(candidate).indexOf(circle) != -1
					&& General.getPlanType(candidate).indexOf(planType) != -1)
				return true;
			else
				return false;
		} else {
			if (searchText != null
					&& General.getPlanOperatorID(candidate).indexOf(operator) != -1
					&& General.getPlanCircleID(candidate).indexOf(circle) != -1)
				return true;
			else
				return false;
		}

	}
}

/*
 * class ConnectionHandler extends Thread {
 * 
 * private LocationProvider locationProvider = null; private Criteria criteria;
 * private boolean isLocationCreated = false; private Location location; private
 * AddressInfo addressInformation; private Coordinates coordinates; private
 * double latitude; private double longitude; private float altitude;
 * 
 * public ConnectionHandler() { }
 * 
 * public void createLocationProvider() { if (locationProvider == null) {
 * criteria = new Criteria();
 * 
 * criteria.setAddressInfoRequired(true); criteria.setAltitudeRequired(true);
 * try { locationProvider = LocationProvider.getInstance(criteria);
 * isLocationCreated = true;
 * 
 * } catch (Exception le) {
 * 
 * Dialog.show("SOrry", "Cannot create LocationProvider for this criteria",
 * "Ok", null); isLocationCreated = false; // le.printStackTrace(); } } }
 * 
 * public void run() { try { createLocationProvider();
 * 
 * if ((locationProvider == null)) {
 * 
 * Dialog.show("SOrry", "Cannot run without location provider!", "Ok", null);
 * return; } else {
 * 
 * location = locationProvider.getLocation(Location.MTE_SATELLITE);
 * addressInformation = location.getAddressInfo();// this is for // getting the
 * // address of // the location coordinates =
 * location.getQualifiedCoordinates();// this gives // the // locations //
 * Coordinates
 * 
 * String[] array = new String[5]; array[0] =
 * addressInformation.getField(AddressInfo.EXTENSION); array[1] =
 * addressInformation.getField(AddressInfo.STREET); array[2] =
 * addressInformation.getField(AddressInfo.COUNTY); array[3] =
 * addressInformation.getField(AddressInfo.CITY); array[4] =
 * addressInformation.getField(AddressInfo.STATE);
 * 
 * // address of the location StringItem address = new StringItem("Address :",
 * new StringBuffer(array[0] + "\n" + array[1] + "\n" + array[2] + "\n" +
 * array[3] + "\n" + array[4]) .toString());
 * 
 * // postal code of the location StringItem postalCode = new
 * StringItem("Postal Code :",
 * addressInformation.getField(AddressInfo.POSTAL_CODE));
 * 
 * // country of the location StringItem country = new StringItem("Country :",
 * addressInformation.getField(AddressInfo.COUNTRY));
 * 
 * System.out.println(address.getText());
 * System.out.println(postalCode.getText());
 * System.out.println(country.getText());
 * 
 * if (coordinates != null) {
 * 
 * latitude = coordinates.getLatitude();// this gives the // latitude of // the
 * location longitude = coordinates.getLongitude();// this gives the //
 * longitude of // the location
 * 
 * StringItem latitudeString = new StringItem("Latitude :", " " + latitude);
 * StringItem longitudeString = new StringItem("Longitude :", " " + longitude);
 * 
 * System.out.println(latitudeString.getText());
 * System.out.println(longitudeString.getText());
 * 
 * }
 * 
 * } // } } catch (LocationException ex) { Dialog.show("Sorry",
 * "Location Cannot Find", "Ok", null); // ex.printStackTrace(); } catch
 * (InterruptedException ex) { // System.out.println("InterruptedException " +
 * ex); // ex.printStackTrace(); } catch (Exception ex) {
 * 
 * // System.out.println("Whole Exception " + ex); }
 * 
 * } }
 */