import java.io.*;
import java.util.*;

public class XMPPXmlWriter {
	private OutputStreamWriter writer;

	private Stack tags;

	boolean inside_tag;

	public XMPPXmlWriter(final OutputStream out)
			throws UnsupportedEncodingException {
		writer = new OutputStreamWriter(out, "UTF-8");
		this.tags = new Stack();
		this.inside_tag = false;
	}

	public void close() {
		try {
			writer.close();
		} catch (IOException e) {
		}
	}

	public void flush() throws IOException {
		if (this.inside_tag) {
			writer.write('>'); // prevent Invalid XML fatal error
			this.inside_tag = false;
		}
		writer.flush();
	}

	public void startTag(final String tag) throws IOException {
		if (this.inside_tag) {
			writer.write('>');
		}

		writer.write('<');
		writer.write(tag);
		this.tags.push(tag);
		this.inside_tag = true;
	}

	public void attribute(final String atr, final String value)
			throws IOException {
		if (value == null) {
			return;
		}
		writer.write(' ');
		writer.write(atr);
		writer.write("=\'");
		this.writeEscaped(value);
		writer.write('\'');
	}

	public void endTag() throws IOException {
		try {
			final String tagname = (String) this.tags.pop();
			if (this.inside_tag) {
				writer.write("/>");
				this.inside_tag = false;
			} else {
				writer.write("</");
				writer.write(tagname);
				writer.write('>');
			}
		} catch (final EmptyStackException e) {
		}
	}

	public void text(final String str) throws IOException {
		if (this.inside_tag) {
			writer.write('>');
			this.inside_tag = false;
		}
		this.writeEscaped(this.encodeUTF(str));
	}

	private void writeEscaped(final String str) throws IOException {
		final int index = 0;
		for (int i = 0; i < str.length(); i++) {
			final char c = str.charAt(i);
			switch (c) {
			case '<':
				writer.write("&lt;");
			case '>':
				writer.write("&gt;");
			case '&':
				writer.write("&amp;");
			case '\'':
				writer.write("&apos;");
			case '"':
				writer.write("&quot;");
			default:
				writer.write(c);
			}
		}
	}

	private String encodeUTF(final String str) {
		try {
			final String utf = new String(str.getBytes("UTF-8"));
			return utf;
		} catch (final UnsupportedEncodingException e) {
			return null;
		}
	}
};
