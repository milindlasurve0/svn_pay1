import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.io.DatagramConnection;
import javax.microedition.io.SocketConnection;
import javax.microedition.io.StreamConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;

public class Test extends Thread {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run() {

		try {
			DatagramConnection dgc = (DatagramConnection) Connector
					.open("datagram://192.168.0.38:8888");
			try {
				byte[] payload = "uuid:sandeep".getBytes();
				Datagram datagram = dgc.newDatagram(payload, payload.length);
				dgc.send(datagram);
				dgc.receive(datagram);
				System.out.println("Out "
						+ new String(datagram.getData()).trim());
			} finally {
				dgc.close();
			}
		} catch (IOException x) {
			x.printStackTrace();
		}

		// InputStream is = null;
		// OutputStream os = null;
		// StreamConnection socket = null;
		//
		// try {
		// String server = "192.168.0.38";
		// String port = "5000";
		// String name = "socket://" + server + ":" + port;
		// socket = (StreamConnection) Connector.open(name,
		// Connector.READ_WRITE);
		//
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// return;
		// }
		//
		// try {
		// // Send a message to the server
		// String request = "Test";
		//
		// os = socket.openOutputStream();
		// os.write(request.getBytes());
		// os.close();
		//
		// // Read the server's reply, up to a maximum
		// // of 128 bytes.
		// is = socket.openInputStream();
		// final int MAX_LENGTH = 128;
		// byte[] buf = new byte[MAX_LENGTH];
		// int total = 0;
		// while (total < MAX_LENGTH) {
		// int count = is.read(buf, total, MAX_LENGTH - total);
		// if (count < 0) {
		// break;
		// }
		// total += count;
		// }
		// is.close();
		// String reply = new String(buf, 0, total);
		// System.out.println("Replay " + reply);
		// socket.close();
		// } catch (IOException ex) {
		// ex.printStackTrace();
		// return;
		// } finally {
		// // Close open streams and the socket
		// try {
		// if (is != null) {
		// is.close();
		// is = null;
		// }
		// } catch (IOException ex1) {
		// }
		// try {
		// if (os != null) {
		// os.close();
		// os = null;
		// }
		// } catch (IOException ex1) {
		// }
		// try {
		// if (socket != null) {
		// socket.close();
		// socket = null;
		// }
		// } catch (IOException ex1) {
		// }
		// }
	}

}
