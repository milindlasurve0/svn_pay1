import java.security.MessageDigest;

public class Auth {
	public static String dataEncryptor(String sInput) {
		try {
			// SHA1 digest length is 160b (40 characters)
			int nDigestLen = 20;
			byte[] PlainText = sInput.getBytes();
			// // Allocate memory for the encrypted text
			byte[] EncryptedText = new byte[nDigestLen];
			MessageDigest Cipher;

			Cipher = MessageDigest.getInstance("SHA-1");
			Cipher.update(PlainText, 0, PlainText.length);
			Cipher.digest(EncryptedText, 0, nDigestLen);
			// } catch (DigestException e) {
			// // TODO: handle exception
			// // e.printStackTrace();
			// } catch (NoSuchAlgorithmException e) {
			// // e.printStackTrace();
			return toHex(EncryptedText);
		} catch (Exception e) {
			// e.printStackTrace();
			return General.getUUID();
		}

	}

	private static String toHex(byte[] bsData) {
		int nDataLen = bsData.length;
		String sHex = "";
		for (int nIter = 0; nIter < nDataLen; nIter++) {
			int nValue = (bsData[nIter] + 256) % 256;
			int nIndex1 = nValue >> 4;
			sHex += Integer.toHexString(nIndex1);
			int nIndex2 = nValue & 0x0f;
			sHex += Integer.toHexString(nIndex2);
		}
		return sHex;
	}
}
